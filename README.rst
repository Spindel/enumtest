========
enumtest
========

Example code running Enum => database Enum and back again


Run with:

$ podman pull postgres
$ podman run -p 5432:5432 -e POSTGRES_PASSWORD='' postgres
$ psql -h 127.0.0.1 -Upostgres -c 'create database "test"' postgres
$ DATABASE_URL="postgresql://postgres@127.0.0.1:5432/test" enumtest


And:

DATABASE_URL="sqlite:///:memory:" enumtest


To compare

