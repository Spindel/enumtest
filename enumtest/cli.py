# -*- coding: utf-8 -*-

"""Console script for enumtest."""

import click
import sqlalchemy as sa
from .models.meta import Base, get_engine
import enumtest.models.mymodel as MyModel


@click.command()
def main(args=None):
    """Console script for enumtest."""
    engine = get_engine()
    click.echo("Running creation...")
    Base.metadata.create_all(engine)

    Session = sa.orm.sessionmaker(bind=engine)
    sess = Session()

    click.echo("done. Inserting some enums")


    ef0 = MyModel.EFirst.zero
    ef1 = MyModel.EFirst.one
    ef2 = MyModel.EFirst.two
    f1 = MyModel.First(value=ef0)
    f2 = MyModel.First(value=ef1)
    f3 = MyModel.First(value=ef2)
    sess.add_all((f1, f2, f3))

    click.echo("Creating some Second enums")

    es0 = MyModel.ESecond.zero
    es1 = MyModel.ESecond.one
    es2 = MyModel.ESecond.two
    es0b = MyModel.ESecond.nil
    es2b = MyModel.ESecond.dos

    s1 = MyModel.Second(value=es0)
    s2 = MyModel.Second(value=es1)
    s3 = MyModel.Second(value=es2)
    s4 = MyModel.Second(value=es0b)
    s5 = MyModel.Second(value=es2b)
    sess.add_all((s1, s2, s3, s4, s5))

    for x in sess.query(MyModel.Second):
        click.echo(x.value)

if __name__ == "__main__":
    main()
