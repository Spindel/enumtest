from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    Enum,
)

from .meta import Base
import enum

class EFirst(enum.Enum):
    zero = 0
    one = 1
    two = 2
    three = 3

class ESecond(enum.Enum):
    zero = 0
    one = 1
    two = 2
    three = 3
    uno = 1
    null = 0
    nil = 0
    dos = 2
    tres = 3


class First(Base):
    __tablename__ = 'first'
    id = Column(Integer, primary_key=True)
    value = Column(Enum(EFirst, name="enum_first"))

class Second(Base):
    __tablename__ = 'second'
    id = Column(Integer, primary_key=True)
    value = Column(Enum(ESecond, name="enum_second"))
